# Wiktionnaire Template Replacer

## Modèles 
### Modèles supportés

Légende :
- ❌: pas de tests pour ce modèle
- 🚧: tests en cours de création
- 💥: tests complets, mais tous ou certains ne passent pas
- ✅: tests complets et tous passent

| Modèle                                                                                         | Wiktionnaire                                                                                                  | Class                                                                                                                                         | Tests                                                                                                              |
|------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------|
| `{{calque}}`                                                                                   | [Modèle:calque](https://fr.wiktionary.org/wiki/Mod%C3%A8le:calque)                                            | [TemplateCalque](src/main/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateCalque.scala)                                     | [✅](src/test/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateCalqueSpec.scala)                   |
| `{{cf}}`                                                                                       | [Modèle:cf](https://fr.wiktionary.org/wiki/Mod%C3%A8le:cf)                                                    | [TemplateCf](src/main/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateCf.scala)                                             | [✅](src/test/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateCfSpec.scala)                       |
| `{{circa}}`                                                                                    | [Modèle:circa](https://fr.wiktionary.org/wiki/Mod%C3%A8le:circa)                                              | [TemplateCirca](src/main/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateCirca.scala)                                       | [✅](src/test/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateCircaSpec.scala)                    |
| `{{composé de}}`<br/>`{{compos}}`<br/>`{{deet}}`                                               | [Modèle:composé de](https://fr.wiktionary.org/wiki/Mod%C3%A8le:compos%C3%A9_de)                               | [TemplateComposeDe](src/main/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateComposeDe.scala)                               | [💥](src/test/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateComposeDeSpec.scala)               |
| `{{date}}`                                                                                     | [Modèle:date](https://fr.wiktionary.org/wiki/Mod%C3%A8le:date)                                                | [TemplateDate](src/main/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateDate.scala)                                         | [✅](src/test/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateDateSpec.scala)                     |
| `{{dénominal}}`                                                                                | [Modèle:dénominal](https://fr.wiktionary.org/wiki/Mod%C3%A8le:d%C3%A9nominal)                                 | [TemplateDenominal](src/main/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateDenominal.scala)                               | [✅](src/test/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateDenominalSpec.scala)                |
| `{{dénominal de}}`                                                                             | [Modèle:dénominal de](https://fr.wiktionary.org/wiki/Mod%C3%A8le:d%C3%A9nominal_de)                           | [TemplateDenominalDe](src/main/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateDenominalDe.scala)                           | [✅](src/test/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateDenominalDeSpec.scala)              |
| `{{e}}`                                                                                        | [Modèle:e](https://fr.wiktionary.org/wiki/Mod%C3%A8le:e)                                                      | [TemplateE](src/main/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateE.scala)                                               | [✅](src/test/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateESpec.scala)                        |
| `{{étyl}}`                                                                                     | [Modèle:étyl](https://fr.wiktionary.org/wiki/Mod%C3%A8le:%C3%A9tyl)                                           | [TemplateEtyl](src/main/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateEtyl.scala)                                         | [✅](src/test/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateEtylSpec.scala)                     |
| `{{exemple}}`                                                                                  | [Modèle:exemple](https://fr.wiktionary.org/wiki/Mod%C3%A8le:exemple)                                          | [TemplateExemple](src/main/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateExemple.scala)                                   | [✅](src/test/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateExempleSpec.scala)                  |
| `{{fchim}}`                                                                                    | [Modèle:fchim](https://fr.wiktionary.org/wiki/Mod%C3%A8le:fchim)                                              | [TemplateFchim](src/main/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateFchim.scala)                                       | [✅](src/test/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateFchimSpec.scala)                    |
| `{{forme pronominale}}`                                                                        | [Modèle:forme pronominale](https://fr.wiktionary.org/wiki/Mod%C3%A8le:forme_pronominale)                      | [TemplateFormePronominale](src/main/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateFormePronominale.scala)                 | [✅](src/test/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateFormePronominaleSpec.scala)         |
| `{{ISBN}}`                                                                                     | [Modèle:ISBN](https://fr.wiktionary.org/wiki/Mod%C3%A8le:ISBN)                                                | [TemplateISBN](src/main/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateISBN.scala)                                         | [✅](src/test/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateISBNSpec.scala)                     |
| `{{lang}}`                                                                                     | [Modèle:lang](https://fr.wiktionary.org/wiki/Mod%C3%A8le:lang)                                                | [TemplateLang](src/main/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateLang.scala)                                         | [✅](src/test/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateLangSpec.scala)                     |
| `{{lien}}`                                                                                     | [Modèle:lien](https://fr.wiktionary.org/wiki/Mod%C3%A8le:lien)                                                | [TemplateLien](src/main/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateLien.scala)                                         | [✅](src/test/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateLienSpec.scala)                     |
| `{{lien-ancre-étym}}`<br/>`{{laé}}`                                                            | [Modèle:lien-ancre-étym](https://fr.wiktionary.org/wiki/Mod%C3%A8le%3Alien%2Dancre%2D%C3%A9tym)               | [TemplateLienAncreEtym](src/main/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateLienAncreEtym.scala)                       | [✅](src/test/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateLienAncreEtymSpec.scala)            |
| `{{phon}}`                                                                                     | [Modèle:phon](https://fr.wiktionary.org/wiki/Mod%C3%A8le:phon)                                                | [TemplatePhon](src/main/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplatePhon.scala)                                         | [✅](src/test/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplatePhonSpec.scala)                     |
| `{{polytonique}}`                                                                              | [Modèle:Polytonique](https://fr.wiktionary.org/wiki/Mod%C3%A8le:Polytonique)                                  | [TemplatePolytonique](src/main/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplatePolytonique.scala)                           | [✅](src/test/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplatePolytoniqueSpec.scala)              |
| `{{prononciation}}`<br/>`{{pron}}`                                                             | [Modèle:prononciation](https://fr.wiktionary.org/wiki/Mod%C3%A8le:prononciation)                              | [TemplatePrononciation](src/main/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplatePrononciation.scala)                       | [✅](src/test/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplatePrononciationSpec.scala)            |
| `{{recons}}`                                                                                   | [Modèle:recons](https://fr.wiktionary.org/wiki/Mod%C3%A8le:recons)                                            | [TemplateRecons](src/main/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateRecons.scala)                                     | [✅](src/test/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateReconsSpec.scala)                   |
| `{{référence nécessaire}}`<br/>`{{refnec}}`<br/>`{{réfnéc}}`<br/>`{{réf ?}}`<br/>`{{source?}}` | [Modèle:référence nécessaire](https://fr.wiktionary.org/wiki/Mod%C3%A8le:r%C3%A9f%C3%A9rence_n%C3%A9cessaire) | [TemplateReferenceNecessaire](src/main/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateReferenceNecessaire.scala)           | [✅](src/test/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateReferenceNecessaireSpec.scala)      |
| `{{siècle}}`                                                                                   | [Modèle:siècle](https://fr.wiktionary.org/wiki/Mod%C3%A8le:si%C3%A8cle)                                       | [TemplateSiecle](src/main/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateSiecle.scala)                                     | [✅](src/test/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateSiecleSpec.scala)                   |
| `{{siècle2}}`                                                                                  | [Modèle:siècle2](https://fr.wiktionary.org/wiki/Mod%C3%A8le:si%C3%A8cle2)                                     | [TemplateSiecle2](src/main/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateSiecle2.scala)                                   | [✅](src/test/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateSiecle2Spec.scala)                  |
| `{{source}}`                                                                                   | [Modèle:source](https://fr.wiktionary.org/wiki/Mod%C3%A8le:source)                                            | [TemplateSource](src/main/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateSource.scala)                                     | [✅](src/test/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateSourceSpec.scala)                   |
| `{{superlatif de}}`                                                                            | [Modèle:superlatif de](https://fr.wiktionary.org/wiki/Mod%C3%A8le:superlatif_de)                              | [TemplateSuperlatifDe](src/main/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateSuperlatifDe.scala)                         | [✅](src/test/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateSuperlatifDeSpec.scala)             |
| `{{term}}`                                                                                     | [Modèle:term](https://fr.wiktionary.org/wiki/Mod%C3%A8le:term)                                                | [TemplateTerm](src/main/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateTerm.scala)                                         | [✅](src/test/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateTermSpec.scala)                     |
| `{{unité}}`                                                                                    | [Modèle:Unité](https://fr.wiktionary.org/wiki/Mod%C3%A8le:Unité)                                              | [TemplateUnité](src/main/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateUnite.scala)                                       | [✅](src/test/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateUniteSpec.scala)                    |
| `{{variante de}}`                                                                              | [Modèle:variante de](https://fr.wiktionary.org/wiki/Mod%C3%A8le:variante_de)                                  | [TemplateVarianteDe](src/main/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateVarianteDe.scala)                             | [✅](src/test/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateVarianteDeSpec.scala)               |
| `{{variante orthographique de}}`<br/>`{{variante ortho de}}`                                   | [Modèle:variante orthographique de](https://fr.wiktionary.org/wiki/Mod%C3%A8le:variante_orthographique_de)    | [TemplateVarianteOrthographiqueDe](src/main/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateVarianteOrthographiqueDe.scala) | [✅](src/test/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateVarianteOrthographiqueDeSpec.scala) |
| `{{w}}`                                                                                        | [Modèle:w](https://fr.wiktionary.org/wiki/Mod%C3%A8le:w)                                                      | [TemplateWikipedia](src/main/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateWikipedia.scala)                               | [💥](src/test/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateWikipediaSpec.scala)               |
| `{{x10}}`                                                                                      | [Modèle:x10](https://fr.wiktionary.org/wiki/Mod%C3%A8le:x10)                                                  | [TemplateX10](src/main/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateX10.scala)                                           | [✅](src/test/scala/fr/mnemotix/wiktionnaire/templatereplacer/templates/TemplateX10Spec.scala)                      |

## À étudier

- `{{abréviation de}}` : obsolete - no longer used
- `{{variante ortho de}}` : "supersedes" `{{lien}}` - is there a way to simplify the code there?

## Planning

| Deadline   | Content                                                                    |
|------------|----------------------------------------------------------------------------|
| 13/05/2022 | Support of all templates currently supported by the Wiktionnaire Annotator |
| 13/05/2022 | TemplateReplacer is packaged and ready to be used as a module              |

## Documentation

### Ajouter un Modèle

L'ajout d'un Modèle dans le Wiktionnaire Template Replacer se déroule en trois étapes :
1. Création de la classe pour le Modèle
2. Ajout du Modèle à la TemplateFactory
3. Création de tests unitaires (ou en 0. si l'on souhaite adopter une approche TDD)

Pour l'exemple, nous allons ajouter le modèle [`{{polytonique}}`](https://fr.wiktionary.org/wiki/Mod%C3%A8le:Polytonique).
Il s'agit d'un modèle dont l'implémentation couvrira l'ensemble des capacités du Template Replacer, et qui est correctement documenté sur le Wiktionnaire.

#### Création de la classe du Modèle

TemplateParameters, required...

format()

#### Ajouter le Modèle à la TemplateFactory

Notre classe `TemplatePolytonique` est prête.
Mais pour pouvoir parser le modèle correspondant (`{{polytonique}}`), nous devons expliciter cette association au TemplateParser.
C'est l'objectif de la [TemplateFactory](src/main/scala/fr/mnemotix/wiktionnaire/templatereplacer/TemplateFactory).

La TemplateFactory est un objet qui dispose d'une unique méthode `#create(...)` qui est appelée par le TemplateParser (qui lui passe alors le nom du modèle qu'il a trouvé, et les paramètres).
C'est dans cette méthode, et plus précisement **dans l'expression match** que nous allons fournir **l'association entre le nom du modèle et la classe**.

Notez qu'à l'entrée de l'expression match, **le nom du modèle est mis en minuscules** pour éliminer toute influence de la casse.

Voici un court extrait de l'expression match :

```scala
name.toLowerCase() match {
  case "circa" => TemplateCirca(params)
  case "étyl" => TemplateEtyl(params)
  case "prononciation" | "pron" => TemplatePrononciation(params)
  case _ => TemplateUndefined(name, params)
}
```

Pour ajouter notre `TemplatePolytonique`, il nous suffit de suivre la norme et d'ajouter la ligne suivante **avant l'expression case "attrape-tout"** :
```scala
case "polytonique" => TemplatePolytonique(params)
```

Ce n'est pas le cas pour `{{polytonique}}`, mais, **parfois, des modèles peuvent avoir plusieurs noms** (des "redirections", dans le jargon wiki).
**Inutile de créer plusieurs classes "dupliquées"**, il suffit de faire comme pour les modèles `{{prononciation}}` ou `{{référence nécessaire}}` :
```scala
case "prononciation" | "pron" => TemplatePrononciation(params)
```

Parfait !
Maintenant, la TemplateFactory pourra fournir une instance de `TemplatePolytonique` au TemplateParser lorsqu'il trouvera le modèle `{{polytonique}}` dans le texte !