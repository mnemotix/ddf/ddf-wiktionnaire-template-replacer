package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.TemplateReplacer
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class TemplateLangSpec extends AnyFlatSpec with Matchers {
  "TemplateLang" should "handle simple words" in {
    val definition = "{{Lang|grc|ἄγω}}"
    TemplateReplacer.replaceTemplates(definition) shouldEqual "ἄγω"
  }

  it should "handle a word with transcription" in {
    val definition1 = "{{Lang|grc|ἄγω|ágō}}"
    val definition2 = "{{lang|ja|骨|tr=hone}}"
    TemplateReplacer.replaceTemplates(definition1) shouldEqual "ἄγω, ágō"
    TemplateReplacer.replaceTemplates(definition2) shouldEqual "骨, hone"
  }

  it should "handle a word with sens" in {
    val def1 = "{{lang|grc|ἄγω|sens=mener}}"
    val def2 = "{{Lang|mnc|ᠠᠨᡳᠶᠠ||année}}"
    TemplateReplacer.replaceTemplates(def1) shouldEqual "ἄγω (« mener »)"
    TemplateReplacer.replaceTemplates(def2) shouldEqual "ᠠᠨᡳᠶᠠ (« année »)"
  }

  it should "handle a word with transcription and sens" in {
    val definition = "{{Lang|mnc|ᠠᠨᡳᠶᠠ|aniya|année}}"
    TemplateReplacer.replaceTemplates(definition) shouldEqual "ᠠᠨᡳᠶᠠ, aniya (« année »)"
  }
}
