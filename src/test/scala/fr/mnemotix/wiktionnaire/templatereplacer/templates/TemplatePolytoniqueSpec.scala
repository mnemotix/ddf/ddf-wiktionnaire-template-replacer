package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.TemplateReplacer
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

// Credit: wiktionnaire-annotator, Pierre-René Lhérisson
class TemplatePolytoniqueSpec extends AnyFlatSpec with Matchers {
  "TemplatePolytonique" should "parse simple polytonique" in {
    val eutopie = "{{polytonique|[[τόπος]]|tópos|lieu}}, signifiant donc « le lieu du bon »."
    TemplateReplacer.replaceTemplates(eutopie) shouldEqual "[[τόπος]], tópos (« lieu »), signifiant donc « le lieu du bon »."
  }

  it should "parse polytonique with lien model inside" in {
    val cephalante = "{{polytonique|{{lien|ἄνθος|grc}}|anthos|fleur}}."
    TemplateReplacer.replaceTemplates(cephalante) shouldEqual "ἄνθος, anthos (« fleur »)."
  }

  it should "parse polytonique with lien model inside and internal link" in {
    val kirrhonose = "{{polytonique|{{lien|νόσος|grc}}|nósos|[[maladie]]}}."
    TemplateReplacer.replaceTemplates(kirrhonose) shouldEqual "νόσος, nósos (« [[maladie]] »)."
  }

  it should "parse two polytonique with lien model inside" in {
    val chorizonte = "de {{polytonique|{{lien|χωρίζω|grc}}|khôrizô|séparer}} et, plus avant, de {{polytonique|{{lien|χωρίς|grc}}|khôris|à part}}."
    TemplateReplacer.replaceTemplates(chorizonte) shouldEqual "de χωρίζω, khôrizô (« séparer ») et, plus avant, de χωρίς, khôris (« à part »)."
  }

  it should "parse a simple polytonique with lien model inside" in {
    val cephalante = "Du grec ancien κεφαλή, kephalê (« tête ») et {{polytonique|{{lien|ἄνθος|grc}}|anthos|fleur}}."
    TemplateReplacer.replaceTemplates(cephalante) shouldEqual "Du grec ancien κεφαλή, kephalê (« tête ») et ἄνθος, anthos (« fleur »)."
  }

  it should "parse polytonique for oaristus" in {
    val oaristus = "Du grec ancien {{polytonique|ὀαριστύς|oaristus}} « entretien tendre, amoureux »."
    TemplateReplacer.replaceTemplates(oaristus) shouldEqual "Du grec ancien ὀαριστύς, oaristus « entretien tendre, amoureux »."
  }
}
