package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.TemplateReplacer
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class TemplateESpec extends AnyFlatSpec with Matchers {
  "TemplateE" should "replace the template if no texte is provided" in {
    val input = "3{{e}}"
    TemplateReplacer.replaceTemplates(input) shouldEqual "3<sup>e</sup>"
  }

  it should "replace the template if texte is provided" in {
    val input = "Fe{{e|2-}}"
    TemplateReplacer.replaceTemplates(input) shouldEqual "Fe<sup>2-</sup>"
  }

  // DDFS-188: Make sure that {{e}} properly handles the "&minus;" in the attocandela definition
  it should "handle an escaped character in texte" in {
    val input = "10{{e|&minus;18}}"
    TemplateReplacer.replaceTemplates(input) shouldEqual "10<sup>&minus;18</sup>"
  }
}
