package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.TemplateReplacer
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class TemplateLienSpec extends AnyFlatSpec with Matchers {
  "TemplateLien" should "markup a link to a french word" in {
    val definition = "{{lien|Sarcellades|fr}}"
    TemplateReplacer.replaceTemplates(definition) shouldEqual "[[Sarcellades]]"
  }

  it should "markup a link to an implicitly french word" in {
    // as of May 2021, the lang parameter is not required and has "fr" as its default value
    // however, this behaviour is likely to change in the coming years, with the "lang" parameter losing its default value
    // and becoming mandatory
    val definition = "{{lien|Sarcellades}}"
    TemplateReplacer.replaceTemplates(definition) shouldEqual "[[Sarcellades]]"
  }

  it should "handle a link to a non-french word" in {
    val definition = "{{lien|pizza|lang=it}}"
    TemplateReplacer.replaceTemplates(definition) shouldEqual "pizza"
  }

  it should "handle a link to a non-french word with sens" in {
    val definition = "{{lien|calliomarcos|lang=gaulois|sens=tussilage}}"
    TemplateReplacer.replaceTemplates(definition) shouldEqual "calliomarcos (« tussilage »)"
  }

  it should "handle a link to a non-french word with dif" in {
    val definition = "{{lien|autrice|frm|dif=en moyen français}}"
    TemplateReplacer.replaceTemplates(definition) shouldEqual "en moyen français"
  }

  it should "handle a link to a non-french word with sens and dif" in {
    val definition = "{{lien|auctrix|la|dif=auctrīx|sens=agente, autrice, fondatrice, {{lien|instigatrice|fr}} », « {{lien|conseillère|fr}} », en droit « garante d’une vente}}"
    TemplateReplacer.replaceTemplates(definition) shouldEqual "auctrīx (« agente, autrice, fondatrice, [[instigatrice]] », « [[conseillère]] », en droit « garante d’une vente »)"
  }
}

