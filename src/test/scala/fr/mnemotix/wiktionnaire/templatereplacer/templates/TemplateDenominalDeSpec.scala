package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.TemplateReplacer
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class TemplateDenominalDeSpec extends AnyFlatSpec with Matchers {
  "TemplateDenominalDe" should "convert a simple model with two words" in {
    val estramaconner = "{{dénominal de|estramaçon|fr}}."
    TemplateReplacer.replaceTemplates(estramaconner) shouldEqual "Dénominal de [[estramaçon]]."
  }

  it should "convert a simple model with renvier" in {
    val plager = "{{dénominal de|lang=fr|plage}}."
    TemplateReplacer.replaceTemplates(plager) shouldEqual "Dénominal de [[plage]]."
  }

  it should "convert a simple model with deverbal" in {
    val lambiner = "(XVIIe siècle) {{dénominal de|lambin|fr}}."
    TemplateReplacer.replaceTemplates(lambiner) shouldEqual "(XVIIe siècle) Dénominal de [[lambin]]."
  }

}

