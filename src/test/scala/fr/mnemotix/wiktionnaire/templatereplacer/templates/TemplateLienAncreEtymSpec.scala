package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.TemplateReplacer
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class TemplateLienAncreEtymSpec extends AnyFlatSpec with Matchers {
  "TemplateLienAncreEtym" should "ignore the template if it does not have at least two params" in {
    val input1 = "{{laé}}"
    val input2 = "{{laé|fr}}"
    TemplateReplacer.replaceTemplates(input1) shouldEqual ""
    TemplateReplacer.replaceTemplates(input2) shouldEqual ""
  }

  it should "handle with a section" in {
    val input = "{{laé|fr|nom}}"
    TemplateReplacer.replaceTemplates(input) shouldEqual "''(Nom commun)''"
  }

  it should "ignore the section number" in {
    val input = "{{laé|br|adj|1}}"
    TemplateReplacer.replaceTemplates(input) shouldEqual "''(Adjectif)''"
    // on Wiktionary, this would have been replaced with ''(Adjectif 1)'', which we don't want here
  }

  it should "handle a section with locution=oui/non" in {
    val input1 = "{{laé|fr|adv|locution=oui}}"
    val input2 = "{{laé|fr|adv|locution=non}}"
    TemplateReplacer.replaceTemplates(input1) shouldEqual "''(Locution adverbiale)''"
    TemplateReplacer.replaceTemplates(input2) shouldEqual "''(Adverbe)''"
  }
}

