package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.TemplateReplacer
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class TemplatePrononciationSpec extends AnyFlatSpec with Matchers {
  "TemplatePrononciation" should "replace the template properly if it has content" in {
    val ecranter = "avec un {{pron|t|fr}} [[euphonique]]"
    TemplateReplacer.replaceTemplates(ecranter) shouldEqual "avec un \\t\\ [[euphonique]]"
  }
}

