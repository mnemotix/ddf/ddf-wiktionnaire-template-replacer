package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.TemplateReplacer
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class TemplateCfSpec extends AnyFlatSpec with Matchers {
  "TemplateCf" should "convert the cf model of wiktionnaire in french" in {
    val cf1 = "Du latin invideo {{cf|lang=fr|in-|video}}"
    val cf2 = "{{cf|pomme|terre}}"
    val cf3 = "{{cf|question|mark|lang=en}}"
    val cf4 = "{{cf|surseoir|sursoir|lang=fr-verb}}"

    TemplateReplacer.replaceTemplates(cf1) shouldEqual "Du latin invideo → voir [[in-]] et [[video]]"
    TemplateReplacer.replaceTemplates(cf2) shouldEqual "→ voir [[pomme]] et [[terre]]"
    TemplateReplacer.replaceTemplates(cf3) shouldEqual "→ voir [[question]] et [[mark]]"
    TemplateReplacer.replaceTemplates(cf4) shouldEqual "→ voir [[surseoir]] et [[sursoir]]"
  }

  it should "convert the cf model with lang=fr in second position" in {
    val cf1 = "{{cf|lang=fr|en son for intérieur}}"
    val cf2 = "{{cf|lang=fr|vergeure}}"
    val cf3 = "{{cf|lang=fr|comme chien et chat}}"

    TemplateReplacer.replaceTemplates(cf1) shouldEqual "→ voir [[en son for intérieur]]"
    TemplateReplacer.replaceTemplates(cf2) shouldEqual "→ voir [[vergeure]]"
    TemplateReplacer.replaceTemplates(cf3) shouldEqual "→ voir [[comme chien et chat]]"
  }

  it should "handle all 16 words" in {
    val cf = "{{cf|assaut|manger|pomme|terre | sac|Scala|Wiktionnaire|horizon | fenêtre|maison|téléphone|programmation | trousse|stylo|nappe|laptop}}"
    TemplateReplacer.replaceTemplates(cf) shouldEqual "→ voir [[assaut]], [[manger]], [[pomme]], [[terre]], [[sac]], " +
      "[[Scala]], [[Wiktionnaire]], [[horizon]], [[fenêtre]], [[maison]], [[téléphone]], [[programmation]], [[trousse]], " +
      "[[stylo]], [[nappe]] et [[laptop]]"
  }

  it should "handle at least 3 words" in {
    val cf = "{{cf|premier|deuxième|dernier}}"
    TemplateReplacer.replaceTemplates(cf) shouldEqual "→ voir [[premier]], [[deuxième]] et [[dernier]]"
  }
}

