package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.TemplateReplacer
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class TemplateWikipediaSpec extends AnyFlatSpec with Matchers {
  "TemplateWikipedia" should "handle a simple link" in {
    val luther =  "{{w|Martin Luther}}"
    TemplateReplacer.replaceTemplates(luther) shouldEqual "[[Martin Luther]]"
  }

  // {{w|zone naturelle d’intérêt écologique faunistique et floristique|Zone naturelle d’intérêt écologique faunistique et floristique}}
  it should "handle a link with a different title" in {
    val znieff = "{{w|zone naturelle d’intérêt écologique faunistique et floristique|Zone naturelle d’intérêt écologique faunistique et floristique}}"
    TemplateReplacer.replaceTemplates(znieff) shouldEqual "[[zone naturelle d’intérêt écologique faunistique et floristique|Zone naturelle d’intérêt écologique faunistique et floristique]]"
  }

  it should "handle a link with no parameters" in {
    // on a page called "floraison"
    val linksToHere = "{{w}}"
    TemplateReplacer.replaceTemplates(linksToHere) shouldEqual "[[floraison]]"
  }

  it should "handle a link to a non-French Wikipedia article" in {
    val luther =  "{{w|Martin Luther|lang=en}}"
    TemplateReplacer.replaceTemplates(luther) shouldEqual "Martin Luther"
  }
}

