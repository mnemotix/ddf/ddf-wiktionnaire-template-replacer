package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.TemplateReplacer
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class TemplateSourceSpec extends AnyFlatSpec with Matchers {
  "TemplateSource" should "not be displayed if 1= is not provided" in {
    val input = "{{source}}"
    TemplateReplacer.replaceTemplates(input) shouldEqual ""
  }

  it should "handle 1=" in {
    val input = "{{source|Gustave Flaubert, ''Bouvard et Pécuchet'', tome 1, chapitre 1, Éditions Conard, Paris, 1910}}"
    TemplateReplacer.replaceTemplates(input) shouldEqual "— (Gustave Flaubert, ''Bouvard et Pécuchet'', tome 1, chapitre 1, Éditions Conard, Paris, 1910)"
  }

  it should "handle 1= with lien= (lien being ignored)" in {
    val input = "{{source|Journal de Lorenzo Létourneau (1899), ''17 Eldorado'', Qualigram/Linguatech, Montréal, 2006|lien=https://www.qualigram.ca/qualigram/index.php?id=12276}}"
    TemplateReplacer.replaceTemplates(input) shouldEqual "— (Journal de Lorenzo Létourneau (1899), ''17 Eldorado'', Qualigram/Linguatech, Montréal, 2006)"
  }
}
