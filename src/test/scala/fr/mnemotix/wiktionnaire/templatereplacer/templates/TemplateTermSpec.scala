package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.TemplateReplacer
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class TemplateTermSpec extends AnyFlatSpec with Matchers {
  "TemplateTerm" should "replace the template" in {
    val term = "{{term|Le sujet est un être animé, l’objet est inanimé}}"
    TemplateReplacer.replaceTemplates(term) shouldEqual "(''Le sujet est un être animé, l’objet est inanimé'')"
  }

  it should "deal with an error in term" in {
    val term = "{{term|}} De l'anglais mahout (« cornac »)"
    TemplateReplacer.replaceTemplates(term) shouldEqual "De l'anglais mahout (« cornac »)"
  }

  it should "not mess with links" in {
    val term = "{{term|[[encan|Encan]]}} Se dit lorsqu’une personne devient propriétaire d’un bien mis à l’enchère."
    TemplateReplacer.replaceTemplates(term) shouldEqual "(''[[encan|Encan]]'') Se dit lorsqu’une personne devient propriétaire d’un bien mis à l’enchère."
  }

  it should "parse term with categorie" in {
    val term = "{{term|Îles|lang=fr|clé=societe}}"
    TemplateReplacer.replaceTemplates(term) shouldEqual "(''Îles'')"
  }

  it should "capitalize the first word (if it's an actual word)" in {
    val input = "{{term|abcd}}"
    TemplateReplacer.replaceTemplates(input) shouldEqual "(''Abcd'')"
  }
}
