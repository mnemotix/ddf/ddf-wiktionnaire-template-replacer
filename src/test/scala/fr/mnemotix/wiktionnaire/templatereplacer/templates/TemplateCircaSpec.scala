package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.TemplateReplacer
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class TemplateCircaSpec extends AnyFlatSpec with Matchers {
  "TemplateCirca" should "replace the template properly if it has content" in {
    val input1 = "Attesté {{circa|1825}}"
    val input2 = "{{circa|1477-1478}}"
    TemplateReplacer.replaceTemplates(input1) shouldEqual "Attesté (c. 1825)"
    TemplateReplacer.replaceTemplates(input2) shouldEqual "(c. 1477-1478)"
  }

  it should "ignore the template if it has no content" in {
    val input = "Je suis une {{circa}} vide !"
    TemplateReplacer.replaceTemplates(input) shouldEqual "Je suis une vide !"
  }
}

