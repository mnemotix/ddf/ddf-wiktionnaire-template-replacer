package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.TemplateReplacer
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class TemplateEtylSpec extends AnyFlatSpec with Matchers {

  "TemplateEtyl" should "handle only languages" in {
    val input = "Du {{étyl|la|fr}} domus"
    TemplateReplacer.replaceTemplates(input) shouldEqual "Du latin domus"
  }

  it should "handle with a mot=" in {
    val input = "Du {{étyl|la|fr|mot=invideo}}"
    TemplateReplacer.replaceTemplates(input) shouldEqual "Du latin invideo"
  }

  it should "handle with a mot= and sens=" in {
    val input = "Du {{étyl|la|fr|mot=invitare|sens= inviter}}."
    TemplateReplacer.replaceTemplates(input) shouldEqual "Du latin invitare (« inviter »)."
  }

  it should "handle with a mot= and tr= and sens=" in {
    val input = "Du {{étyl|grc|en|mot=λόγος|tr=logos|type=nom|sens=étude}}."
    TemplateReplacer.replaceTemplates(input) shouldEqual "Du grec ancien λόγος logos (« étude »)."
  }

  it should "handle with a mot, tr and sens as positional arguments" in {
    val input = "Du {{étyl|grc|en|λόγος|logos|étude|type=nom|lien=1}}."
    TemplateReplacer.replaceTemplates(input) shouldEqual "Du grec ancien λόγος logos (« étude »)."
  }

  it should "handle with a mot, dif and sens" in {
    val input = "venant du {{étyl|la|fr|mot=dictio|dif=dictĭo|sens=mot, expression}}"
    TemplateReplacer.replaceTemplates(input) shouldEqual "venant du latin dictĭo (« mot, expression »)"
  }
}
