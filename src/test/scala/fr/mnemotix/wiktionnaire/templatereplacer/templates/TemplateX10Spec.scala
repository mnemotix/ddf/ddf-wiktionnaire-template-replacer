package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.TemplateReplacer
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class TemplateX10Spec extends AnyFlatSpec with Matchers {
  "TemplateX10" should "handle the template with no value provided" in {
    val input = "1,5{{x10}}"
    TemplateReplacer.replaceTemplates(input) shouldEqual "1,5×10"
  }

  it should "handle the template with an exposant provided" in {
    val input = "1,23{{x10|9}}"
    TemplateReplacer.replaceTemplates(input) shouldEqual "1,23×10<sup>9</sup>"
  }
}
