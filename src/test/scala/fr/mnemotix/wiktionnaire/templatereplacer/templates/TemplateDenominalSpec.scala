package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.TemplateReplacer
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class TemplateDenominalSpec extends AnyFlatSpec with Matchers {
  "TemplateDenominal" should "handle the template if it has no de=" in {
    val input = "{{dénominal|fr}}"
    TemplateReplacer.replaceTemplates(input) shouldEqual "dénominal"
  }

  it should "handle m=1 with no de=" in {
    val input = "{{dénominal|fr|m=1}}"
    TemplateReplacer.replaceTemplates(input) shouldEqual "Dénominal"
  }

  it should "handle de=" in {
    val input = "{{dénominal|de=estramaçon|fr}}"
    TemplateReplacer.replaceTemplates(input) shouldEqual "dénominal de [[estramaçon]]"
  }

  it should "handle de= with nolien=1" in {
    val input = "{{dénominal|de=plage|lang=fr|nolien=1}}"
    TemplateReplacer.replaceTemplates(input) shouldEqual "dénominal de plage"
  }

  it should "handle de= with nolien=1 and texte=" in {
    val input = "{{dénominal|de=larbin|lang=fr|nolien=1|texte=larbine}}"
    TemplateReplacer.replaceTemplates(input) shouldEqual "dénominal de larbin"
  }

  it should "handle de= with texte=" in {
    val input = "{{dénominal|de=larbin|lang=fr|texte=larbine}}"
    TemplateReplacer.replaceTemplates(input) shouldEqual "dénominal de [[larbin|larbine]]"
  }

  it should "handle de= with m=1" in {
    val input = "{{dénominal|fr|de=maison|m=1}}"
    TemplateReplacer.replaceTemplates(input) shouldEqual "Dénominal de [[maison]]"
  }
}

