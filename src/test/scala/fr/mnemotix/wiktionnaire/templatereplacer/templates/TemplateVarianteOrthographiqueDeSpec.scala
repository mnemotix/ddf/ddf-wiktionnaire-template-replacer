package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.TemplateReplacer
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class TemplateVarianteOrthographiqueDeSpec extends AnyFlatSpec with Matchers {
  "TemplateVarianteOrthographiqueDeDe" should "replace variante orthographique in achaine definition" in {
    val  defitionAchaine = "(Botanique) (Rare) {{variante ortho de|akène}}."
    TemplateReplacer.replaceTemplates(defitionAchaine) shouldEqual "(Botanique) (Rare) Variante orthographique de [[akène]]."
  }

  it should "replace variante orthographique in acupuncture" in {
    val acuponcture = "{{variante ortho de|acupuncture}}."
    TemplateReplacer.replaceTemplates(acuponcture) shouldEqual "Variante orthographique de [[acupuncture]]."
  }

  it should "replace variante orthographique in dizenier" in {
    val dizenier =  "{{variante orthographique de|dizenier}}"
    TemplateReplacer.replaceTemplates(dizenier) shouldEqual "Variante orthographique de [[dizenier]]"
  }

  it should "replace using dif if specified" in {
    //TODO see if variante ortho could "depend" on {{lien}}
    val input = "{{variante ortho de|Me|fr|dif=M<sup>e</sup>}}"
    TemplateReplacer.replaceTemplates(input) shouldEqual "Variante orthographique de [[Me]]"
  }

  // DDFS-210 - {{Variante ortho de}} redirects to {{variante ortho de}}
  it should "not mind if the name of the template starts with uppercase" in {
    val acuponcture = "{{Variante ortho de|climatosceptique}}."
    TemplateReplacer.replaceTemplates(acuponcture) shouldEqual "Variante orthographique de [[climatosceptique]]."
  }
}
