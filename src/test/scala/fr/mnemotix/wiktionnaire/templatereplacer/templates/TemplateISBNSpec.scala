package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.TemplateReplacer
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class TemplateISBNSpec extends AnyFlatSpec with Matchers {
  "TemplateISBN" should "handle a single ISBN" in {
    val input = "la direction d’Alain Rey, éditions Le Robert, Paris 19921, {{ISBN|2-85036-187-9}}, p. 1574."
    TemplateReplacer.replaceTemplates(input) shouldEqual "la direction d’Alain Rey, éditions Le Robert, Paris 19921, 2-85036-187-9, p. 1574."
  }

  it should "handle two ISBNs" in {
    val input = "{{ISBN|978-1-23-456789-7|2-876-54301-X}}"
    TemplateReplacer.replaceTemplates(input) shouldEqual "978-1-23-456789-7 et 2-876-54301-X"
  }

  it should "handle three ISBNs" in {
    val input = "{{ISBN|978-1-23-456789-7|2-876-54301-X|2-876-54301-XYZ}}"
    TemplateReplacer.replaceTemplates(input) shouldEqual "978-1-23-456789-7, 2-876-54301-X et 2-876-54301-XYZ"
  }

  it should "ignore the template if it has no content" in {
    val input = "Je suis une {{ISBN}} vide !"
    TemplateReplacer.replaceTemplates(input) shouldEqual "Je suis une vide !"
  }
}

