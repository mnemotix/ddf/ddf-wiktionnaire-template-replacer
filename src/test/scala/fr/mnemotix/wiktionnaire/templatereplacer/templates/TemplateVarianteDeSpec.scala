package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.TemplateReplacer
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class TemplateVarianteDeSpec extends AnyFlatSpec with Matchers {
  "TemplateVarianteDe" should "replace variante in achaine definition" in {
    val  defitionAchaine = "(Botanique) (Rare) {{variante de|akène}}."
    TemplateReplacer.replaceTemplates(defitionAchaine) shouldEqual "(Botanique) (Rare) Variante de [[akène]]."
  }

  it should "replace variante in acupuncture" in {
    val acuponcture = "{{variante de|acupuncture}}."
    TemplateReplacer.replaceTemplates(acuponcture) shouldEqual "Variante de [[acupuncture]]."
  }

  it should "replace using dif if specified" in {
    //TODO see if variante could "depend" on {{lien}}
    val input = "{{variante de|Me|fr|dif=M<sup>e</sup>}}"
    TemplateReplacer.replaceTemplates(input) shouldEqual "Variante de [[Me]]"
  }
}
