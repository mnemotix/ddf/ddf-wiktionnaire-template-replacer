package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.TemplateReplacer
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class TemplateReconsSpec extends AnyFlatSpec with Matchers {
  "TemplateRecons" should "handle an empty template" in {
    val definition = "{{recons}}''vultorem''"
    TemplateReplacer.replaceTemplates(definition) shouldEqual "<sup>*</sup>''vultorem''"
  }

  it should "handle a word" in {
    val definition = "{{recons|bebros|gaulois}}"
    TemplateReplacer.replaceTemplates(definition) shouldEqual "<sup>*</sup>bebros"
  }

  it should "handle a word with sens" in {
    val definition = "{{recons|maruos|gaul|sens=mort}}"
    TemplateReplacer.replaceTemplates(definition) shouldEqual "<sup>*</sup>maruos (« mort »)"
  }

  it should "handle a word with transcription" in {
    val definition = "{{recons|maruos|gaul|tr=abcdef}}"
    TemplateReplacer.replaceTemplates(definition) shouldEqual "<sup>*</sup>maruos (abcdef)"
  }

  it should "handle a word with transcription and sens" in {
    val definition = "{{recons|maruos|gaul|tr=abcdef|sens=mort}}"
    TemplateReplacer.replaceTemplates(definition) shouldEqual "<sup>*</sup>maruos (abcdef) (« mort »)"
  }
}
