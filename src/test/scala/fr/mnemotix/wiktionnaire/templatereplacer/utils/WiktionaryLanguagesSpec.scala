package fr.mnemotix.wiktionnaire.templatereplacer.utils

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

case class WiktionaryLanguagesSpec() extends AnyFlatSpec with Matchers {
  "WiktionaryLanguages" should "be loaded from the file" in {
    WiktionaryLanguages.langs.isEmpty shouldBe false
  }

  it should "contain Old French" in {
    WiktionaryLanguages.langs.contains("fro") shouldBe true
  }

  it should "have the name for frm" in {
    WiktionaryLanguages.langs.get("frm") shouldEqual Some("moyen français")
  }
}