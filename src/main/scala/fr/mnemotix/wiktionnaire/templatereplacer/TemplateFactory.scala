package fr.mnemotix.wiktionnaire.templatereplacer

import fr.mnemotix.wiktionnaire.templatereplacer.exceptions.TemplateMissingRequiredParametersException
import fr.mnemotix.wiktionnaire.templatereplacer.templates._

object TemplateFactory {
  def create(name: String, params: Map[String, String]): Template = {
    try {
      name.toLowerCase() match {
        case "calque" => TemplateCalque(params)
        case "cf" => TemplateCf(params)
        case "circa" => TemplateCirca(params)
        case "composé de" | "compos" | "deet" => TemplateComposeDe(params)
        case "dénominal" => TemplateDenominal(params)
        case "dénominal de" => TemplateDenominalDe(params)
        case "e" => TemplateE(params)
        case "étyl" => TemplateEtyl(params)
        case "exemple" => TemplateExemple(params)
        case "fchim" => TemplateFchim(params)
        case "forme pronominale" => TemplateFormePronominale(params)
        case "recons" => TemplateRecons(params)
        case "phon" => TemplatePhon(params)
        case "lien" => TemplateLien(params)
        case "lien-ancre-étym" | "laé" => TemplateLienAncreEtym(params)
        case "polytonique" => TemplatePolytonique(params)
        case "isbn" => TemplateISBN(params)
        case "w" => TemplateWikipedia(params)
        case "siècle" => TemplateSiecle(params)
        case "siècle2" => TemplateSiecle2(params)
        case "source" => TemplateSource(params)
        case "date" => TemplateDate(params)
        case "lang" => TemplateLang(params)
        case "prononciation" | "pron" => TemplatePrononciation(params)
        case "référence nécessaire" | "réfnéc" | "refnec" | "source?" | "réf ?" => TemplateReferenceNecessaire(params)
        case "superlatif de" => TemplateSuperlatifDe(params)
        case "unité" => TemplateUnite(params)
        case "variante orthographique de" | "variante ortho de" => TemplateVarianteOrthographiqueDe(params)
        case "variante de" => TemplateVarianteDe(params)
        case "term" => TemplateTerm(params)
        case "x10" => TemplateX10(params)
        case "" => TemplateIgnored() // Ignore any empty template ({{}})
        case _ => TemplateUndefined(name, params)
      }
    } catch {
      case _: TemplateMissingRequiredParametersException => TemplateIgnored() // templates that do not have their required parameters should be ignored
    }
  }
}
