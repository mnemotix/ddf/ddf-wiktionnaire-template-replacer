package fr.mnemotix.wiktionnaire.templatereplacer.exceptions

/**
 * Generic exception thrown by the TemplateReplacer
 */
class TemplateReplacerException extends RuntimeException {

}
