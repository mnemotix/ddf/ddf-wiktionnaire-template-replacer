package fr.mnemotix.wiktionnaire.templatereplacer.utils

case object BooleanLiterals {

  /**
   * Returns the boolean value of the String.
   * @param s the String to get the boolean value of.
   * @return the boolean value of the String.
   */
  def toBoolean(s: String): Boolean = s.toLowerCase match {
    case "oui" | "yes" | "1" => true // afaik, only these values mean "true"
    case _ => false
  }
}
