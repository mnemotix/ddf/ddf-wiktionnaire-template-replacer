package fr.mnemotix.wiktionnaire.templatereplacer.utils

import scala.io.Source

case object WiktionaryLanguages {
  /**
   * Lists all available languages in the Wiktionnaire, as language code -> language name key-value pairs.
   */
  lazy val langs: Map[String, String] = load

  private def load: Map[String, String] = {
    Source.fromResource("codesDeLangues.tsv").getLines().map { line =>
      val tab = line.split("\t")
      (tab(1), tab(2))
    }.toMap
  }
}
