package fr.mnemotix.wiktionnaire.templatereplacer.utils

import scala.util.matching.Regex

/**
 * Provides utilities to handle roman numerals.
 */
case object RomanNumerals {
  /**
   * Matches roman numerals in a String.
   * There is no group in this pattern, as the whole match contains only the roman numeral (sometimes referred to as "group 0").
   * This Regex uses lookarounds, so make sure the pattern matching engine supports them (Java does by default).
   */
  val regex: Regex = "(?<!\\w|[.'<>])M{0,4}(?>CM|CD|D?C{0,3})(?>XC|XL|L?X{0,3})(?>IX|IV|V?I{0,3})\\b(?!\\w|[.'<>])".r

  /**
   * Converts this integer into a String of roman numerals (works from 1 to 3999)
   * @param number the value to convert into roman numerals
   * @return the number in roman numerals as a String
   * @throws IllegalArgumentException if this number cannot be converted into roman numerals
   */
  def fromArabicNumerals( number: Int) : String = {
    if (number <= 0 || number >= 4000) throw new IllegalArgumentException("Cannot convert a number out of the ]0;4000[ range")

    // Source: https://gist.github.com/AndyBowes/3048075
    fromArabicNumerals( number, List( ("M", 1000),("CM", 900), ("D", 500), ("CD", 400), ("C", 100), ("XC", 90),
      ("L", 50), ("XL",40), ("X", 10), ("IX", 9), ("V", 5), ("IV", 4), ("I", 1) ))
  }

  private def fromArabicNumerals( number: Int, digits: List[(String, Int)] ) : String = digits match {
    // Source: https://gist.github.com/AndyBowes/3048075
    case Nil => ""
    case h :: t => h._1 * ( number / h._2 ) + fromArabicNumerals( number % h._2, t )
  }
}
