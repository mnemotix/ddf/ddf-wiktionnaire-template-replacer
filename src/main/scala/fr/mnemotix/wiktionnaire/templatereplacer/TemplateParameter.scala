package fr.mnemotix.wiktionnaire.templatereplacer

/**
 *
 * @param key
 * @param aliases
 * @param required whether the Template should be ignored if no value is provided for this parameter.
 *                 If set to true, it ensures that:
 *                 <ul>
 *                  <li>params(key) is available for this Template (no need to do params.get(key))</li>
 *                  <li>if no value is provided for key, this Template will be replaced by "".</li>
 *                 </ul>
 */
case class TemplateParameter(key: String, aliases: List[String] = List(), required: Boolean = false)
