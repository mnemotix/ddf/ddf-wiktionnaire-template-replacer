package fr.mnemotix.wiktionnaire.templatereplacer

import scala.annotation.tailrec

object TemplateReplacer {
  /**
   * Recursively replaces all known templates in the given String.
   * @param s The String from which to match and replace all the templates it contains.
   * @return The resulting String
   */
  @tailrec
  def replaceTemplates(s: String): String = {
    val modelePattern = "\\{\\{([^{}]*)}}".r // Finds all inner-most templates, and captures their content in group 1.
    if (!(s.contains("{{") && s.contains("}}"))) {
      s.replaceAll("\\s{2,}", " ") // It's time to replace any amount of "multiple spaces" with a single space!
        .trim // and to trim it!
    } else
      replaceTemplates(modelePattern.replaceAllIn(s, m => TemplateParser.parse(TemplateParser.template, m.group(1)).get.format()))
  }
}
