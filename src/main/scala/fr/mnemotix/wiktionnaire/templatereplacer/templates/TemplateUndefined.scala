package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.Template

case class TemplateUndefined (name: String, parameters: Map[String, String])
  extends Template(List(), parameters) {

  override def format(): String = {
    s"!!undefined:$name!!" // Prints out the name of the template with a prefix used for testing
  }
}
