package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.{Template, TemplateParameter}

/**
 * Modèle:recons
 *
 * @param parameters
 */
case class TemplateRecons(parameters: Map[String, String])
  extends Template(
    List(
      TemplateParameter("1"),
      TemplateParameter("2", List("lang")),
      TemplateParameter("tr"),
      TemplateParameter("sens"),
      TemplateParameter("lang-mot-vedette")
    ),
    parameters) {

  /**
   * Le terme reconstitué.
   * Exemple: vultorem
   */
  val mot: Option[String] = params.get("1")
  /**
   * Code de langue du terme reconstitué.
   * Exemple: la
   */
  val lang: Option[String] = params.get("2")
  /**
   * La translitération du terme reconstruit, uniquement s’il n’utilise pas l’alphabet latin.
   */
  val transcript: Option[String] = params.get("tr")
  /**
   * Le sens du terme reconstruit.
   */
  val sens: Option[String] = params.get("sens")

  override def format(): String = {
    mot match {
      case Some(m) => s"<sup>*</sup>$m${if (transcript.isDefined) s" (${transcript.get})" else ""}${if (sens.isDefined) s" (« ${sens.get} »)" else ""}"
      case None => "<sup>*</sup>"
    }
  }
}
