package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.utils.WiktionaryLanguages
import fr.mnemotix.wiktionnaire.templatereplacer.{Template, TemplateParameter}

/**
 * Modèle:calque
 *
 * @param parameters
 */
case class TemplateCalque(parameters: Map[String, String])
  extends Template(
    List(
      TemplateParameter("1", required=true),
      TemplateParameter("2", required=true),
      TemplateParameter("3", aliases = List("mot")),
      TemplateParameter("type"),
      TemplateParameter("num"),
      TemplateParameter("dif"),
      TemplateParameter("4", aliases = List("tr", "R")),
      TemplateParameter("5", aliases = List("sens"))
      //there's also nocat, but it's ignored
    ),
    parameters) {

  /**
   * La langue d'origine
   */
  val languageFrom: String = params("1")
  /**
   * La langue cible
   */
  val languageTo: String = params("2")
  /**
   * Le mot calqué
   */
  val word: Option[String] = params.get("3")
  /**
   * Le type du mot calqué, pour le lien.
   * Exemple : nom
   */
  val sectionType: Option[String] = params.get("type")
  /**
   * Le numéro de la section pour le lien, dans le cas de plusieurs sections de même type dans la page du mot calqué.
   * Exemple : 1
   */
  val sectionNum: Option[String] = params.get("num")
  /**
   * Le texte à afficher à la place du mot calqué
   */
  val texte: Option[String] = params.get("dif")
  /**
   * La transcription du terme calqué, uniquement s’il n’est pas écrit avec l’alphabet latin.
   */
  val transcription: Option[String] = params.get("4")
  /**
   * Le sens du mot calqué.
   */
  val sens: Option[String] = params.get("5")

  override def format(): String = {
    val language = WiktionaryLanguages.langs.getOrElse(languageFrom, "inconnu")
    if (word.isDefined) {
      language + " " + word.get + {
        if (transcription.isDefined) " " + transcription.get
        else ""
      } + {
        if (sens.isDefined) " (« " + sens.get + " »)"
        else ""
      }
    }
    else {
      // no word, it's just the language
      language
    }
  }
}
