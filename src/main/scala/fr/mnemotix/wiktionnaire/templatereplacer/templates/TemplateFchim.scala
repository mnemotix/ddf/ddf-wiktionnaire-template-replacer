package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.{Template, TemplateParameter}

/**
 * Modèle:fchim
 * @param parameters
 */
case class TemplateFchim(parameters: Map[String, String])
  extends Template(
    List(
      // can go up to 20 so far
      TemplateParameter("1"), // element1
      TemplateParameter("2"), // indice1
      TemplateParameter("3"), // element2
      TemplateParameter("4"), // indice2
      TemplateParameter("5"), // etc.
      TemplateParameter("6"),
      TemplateParameter("7"),
      TemplateParameter("8"),
      TemplateParameter("9"),
      TemplateParameter("10"),
      TemplateParameter("11"),
      TemplateParameter("12"),
      TemplateParameter("13"),
      TemplateParameter("14"),
      TemplateParameter("15"),
      TemplateParameter("16"),
      TemplateParameter("17"),
      TemplateParameter("18"),
      TemplateParameter("19"),
      TemplateParameter("20"),
      TemplateParameter("lien")
    ),
    parameters) {

  /**
   * Permet de faire un lien de la formule vers l’article indiqué
   */
  val lien: Option[String] = params.get("lien")

  val allComponents: List[(String, Option[String])] = (1 to 20 by 2).map(i => (params.get(i.toString), params.get((i+1).toString)))
    .filter(_._1.isDefined)
    .map(tuple => (tuple._1.get, tuple._2))
    .toList

  override def format(): String = {
    // If there's no params, returns an empty string
    if (allComponents.isEmpty) ""
    else buildChemicalFormula(allComponents)
  }

  /**
   * Takes the (element, indice) tuples as input to build the chemical formula
   * @param remainingComponents list of remaining tuples to go through
   * @return
   */
  private def buildChemicalFormula(remainingComponents: List[(String, Option[String])]): String = {
    remainingComponents match {
      case Nil => ""
      case last :: Nil => formatTuple(last)
      case h :: t => formatTuple(h) + buildChemicalFormula(t)
    }
  }

  /**
   * Formats a tuple into proper chemical formula element.
   * @param tuple (element, indice) tuple.
   * @return
   */
  private def formatTuple(tuple: (String, Option[String])): String = {
    tuple._1 + {
      if (tuple._2.isDefined) s"<sub>${tuple._2.get}</sub>"
      else ""
    }
  }
}
