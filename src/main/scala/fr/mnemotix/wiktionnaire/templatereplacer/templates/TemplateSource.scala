package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.{Template, TemplateParameter}

/**
 * Modèle:source
 *
 * @param parameters
 */
case class TemplateSource(parameters: Map[String, String])
  extends Template(
    List(
      TemplateParameter("1", required = true),
      TemplateParameter("lien") //ignored
    ),
    parameters) {

  /**
   * Nom de l’auteur, ''titre de l’ouvrage'', année de publication (séparés par des virgules).
   */
  val source: String = params("1")

  override def format(): String = s"— ($source)"
}
