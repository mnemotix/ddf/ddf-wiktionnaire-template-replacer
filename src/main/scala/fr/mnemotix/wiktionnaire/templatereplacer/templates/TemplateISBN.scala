package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.{Template, TemplateParameter}

/**
 * Modèle:ISBN
 *
 * @param parameters
 */
case class TemplateISBN(parameters: Map[String, String])
  extends Template(
    List(
      TemplateParameter("1", required=true), // at least one ISBN, otherwise just ignore the template
      TemplateParameter("2"),
      TemplateParameter("3") // only supports up to three ISBNs
    ),
    parameters) {

  val isbn1: String = params("1")
  val isbn2: Option[String] = params.get("2")
  val isbn3: Option[String] = params.get("3")

  override def format(): String = {
    // not the greatest, but that does the job!
    if (isbn2.isDefined) {
      if (isbn3.isDefined) {
        s"$isbn1, ${isbn2.get} et ${isbn3.get}"
      }
      else s"$isbn1 et ${isbn2.get}"
    }
    else s"$isbn1"
  }
}
