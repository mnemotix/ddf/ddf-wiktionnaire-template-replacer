package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.utils.BooleanLiterals
import fr.mnemotix.wiktionnaire.templatereplacer.{Template, TemplateParameter}

/**
 * Modèle:composé de
 *
 * @param parameters
 */
case class TemplateComposeDe(parameters: Map[String, String])
  extends Template(
    List(
      // up to seven words
      TemplateParameter("1", required=true),
      TemplateParameter("2"),
      TemplateParameter("3"),
      TemplateParameter("4"),
      TemplateParameter("5"),
      TemplateParameter("6"),
      TemplateParameter("7"),

      // Global language
      TemplateParameter("lang"),

      // Or particular values for each one of the words
      TemplateParameter("lang1"),
      TemplateParameter("tr1"),
      TemplateParameter("sens1"),
      TemplateParameter("lang2"),
      TemplateParameter("tr2"),
      TemplateParameter("sens2"),
      TemplateParameter("lang3"),
      TemplateParameter("tr3"),
      TemplateParameter("sens3"),
      TemplateParameter("lang4"),
      TemplateParameter("tr4"),
      TemplateParameter("sens4"),
      TemplateParameter("lang5"),
      TemplateParameter("tr5"),
      TemplateParameter("sens5"),
      TemplateParameter("lang6"),
      TemplateParameter("tr6"),
      TemplateParameter("sens6"),
      TemplateParameter("lang7"),
      TemplateParameter("tr7"),
      TemplateParameter("sens7"),

      TemplateParameter("sens"),

      // and some modifiers
      TemplateParameter("m"),
      TemplateParameter("f")

      //there's also nocat, but it's ignored
    ),
    parameters) {

  val word1: String = params("1")
  val word2: Option[String] = params.get("2")
  val word3: Option[String] = params.get("3")
  val word4: Option[String] = params.get("4")
  val word5: Option[String] = params.get("5")
  val word6: Option[String] = params.get("6")
  val word7: Option[String] = params.get("7")

  /**
   * La langue de l’étymologie.
   * Si les étymologies des mots diffèrent de lang, utilisez lang1, lang2, etc.
   */
  val lang: Option[String] = params.get("lang")

  /**
   * Langue du premier mot.
   */
  val lang1: Option[String] = params.get("lang1")
  /**
   * Transcription ou translittération du premier mot.
   */
  val tr1: Option[String] = params.get("tr1")
  /**
   * Sens du premier mot. (donne une signification pour les mots étrangers)
   */
  val sens1: Option[String] = params.get("sens1")

  /**
   * Langue du deuxième mot.
   */
  val lang2: Option[String] = params.get("lang2")
  /**
   * Transcription ou translittération du deuxième mot.
   */
  val tr2: Option[String] = params.get("tr2")
  /**
   * Sens du deuxième mot.
   */
  val sens2: Option[String] = params.get("sens2")

  /**
   * Langue du troisième mot.
   */
  val lang3: Option[String] = params.get("lang3")
  /**
   * Transcription ou translittération du troisième mot.
   */
  val tr3: Option[String] = params.get("tr3")
  /**
   * Sens du troisième mot.
   */
  val sens3: Option[String] = params.get("sens3")

  /**
   * Langue du quatrième mot.
   */
  val lang4: Option[String] = params.get("lang4")
  /**
   * Transcription ou translittération du quatrième mot.
   */
  val tr4: Option[String] = params.get("tr4")
  /**
   * Sens du quatrième mot.
   */
  val sens4: Option[String] = params.get("sens4")

  /**
   * Langue du cinquième mot.
   */
  val lang5: Option[String] = params.get("lang5")
  /**
   * Transcription ou translittération du cinquième mot.
   */
  val tr5: Option[String] = params.get("tr5")
  /**
   * Sens du cinquième mot.
   */
  val sens5: Option[String] = params.get("sens5")

  /**
   * Langue du sixième mot.
   */
  val lang6: Option[String] = params.get("lang6")
  /**
   * Transcription ou translittération du sixième mot.
   */
  val tr6: Option[String] = params.get("tr6")
  /**
   * Sens du sixième mot.
   */
  val sens6: Option[String] = params.get("sens6")

  /**
   * Langue du septième mot.
   */
  val lang7: Option[String] = params.get("lang7")
  /**
   * Transcription ou translittération du septième mot.
   */
  val tr7: Option[String] = params.get("tr7")
  /**
   * Sens du septième mot.
   */
  val sens7: Option[String] = params.get("sens7")

  /**
   * Sens littéral de tous ces mots qui composent l'étymologie de cet entrée.
   * Exemple : celui qui guide le peuple
   */
  val sensLitteral: Option[String] = params.get("sens")

  /**
   * Met une capitale au premier mot : « Composé de », « Dérivé de ».
   */
  val majuscule: Boolean = BooleanLiterals.toBoolean(params.getOrElse("m", "0"))

  /**
   * Accorde le verbe au féminin : « composée de », « dérivée de ».
   */
  val feminin: Boolean = BooleanLiterals.toBoolean(params.getOrElse("f", "0"))

  /**
   * Lists all defined words (at least word has been provided) as a tuple contained their related lang, tr and sens.
   * Tuples in this list follow this format: (word, lang, tr, sens).
   */
  private val allDefinedWords: List[(String, Option[String], Option[String], Option[String])] = {
    (1 to 7)
      .map(i => (params.get(i.toString), params.get("lang" + i), params.get("tr" + i), params.get("sens" + i)))
      .filter(_._1.isDefined)
      .map(t => (t._1.get, t._2, t._3, t._4))
      .toList
      .sortWith((_, tuple) => isPrefix(tuple._1) || isSuffix(tuple._1)) // if 2 is a prefix or suffix, then exchange 1 <-> 2
  }

  /**
   * Whether this template prints a "dérivé de" rather than "composé de"
   */
  val deriveDe: Boolean = allDefinedWords.exists(t => isPrefix(t._1) || isSuffix(t._1))

  override def format(): String = {
    println(allDefinedWords)
    // We start with the start
    capitalize(start) +
      " " +
      joinWords() + {
        if (sensLitteral.isDefined) s", littéralement « ${sensLitteral.get} »"
        else ""
    }
  }

  private def joinWords(): String = {
    formatTuple(allDefinedWords.head) + joinWords(allDefinedWords.tail)
  }

  private def joinWords(remainingWords: List[(String, Option[String], Option[String], Option[String])]): String = {
    remainingWords match {
      case Nil => ""
      case last :: Nil => formatWord(last, last=true)
      case head :: tail => formatWord(head, last=false) + joinWords(tail)
    }
  }

  private def formatWord(tuple: (String, Option[String], Option[String], Option[String]), last: Boolean): String = {
    if (isSuffix(tuple._1)) s" avec le suffixe ${formatTuple(tuple)}"
    else if (isPrefix(tuple._1)) s" avec le préfixe ${formatTuple(tuple)}"
    else if (last) s" et de ${formatTuple(tuple)}"
    else s", ${formatTuple(tuple)}"
  }

  private def formatTuple(tuple: (String, Option[String], Option[String], Option[String])): String = {
    tuple._1 + {
      // tr
      if (tuple._3.isDefined) s" ''${tuple._3.get}''"
      else ""
    } + {
      // sens
      if (tuple._4.isDefined) s" (« ${tuple._4.get} »),"
      else ""
    }
  }

  /**
   * Returns true if the word is a suffix.
   * E.g.: -in, -ment
   * @param word the word to check.
   * @return true if the word is a suffix, false otherwise.
   */
  private def isSuffix(word: String): Boolean = word.startsWith("-")

  /**
   * Returns true if the word is a prefix.
   * E.g.: re-, dé-
   * @param word the word to check.
   * @return true if the word is a prefix, false otherwise.
   */
  private def isPrefix(word: String): Boolean = word.endsWith("-")

  private val start: String = {
    if (deriveDe) {
      if (feminin) "dérivée de"
      else "dérivé de"
    } else {
      if (feminin) "composée de"
      else "composé de"
    }
  }

  /**
   * Returns this string with the first letter converted to uppercase if majuscule is true.
   * This string is left unchanged otherwise, or if the first letter is already uppercase.
   * @param s the String to capitalize.
   * @return the capitalized String.
   */
  private def capitalize(s: String): String = {
    if (majuscule) s.capitalize
    else s
  }
}
