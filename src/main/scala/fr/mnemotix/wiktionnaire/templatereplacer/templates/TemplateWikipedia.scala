package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.{Template, TemplateParameter}

/**
 * Modèle:w
 * @param parameters
 */
case class TemplateWikipedia(parameters: Map[String, String])
  extends Template(
    List(
      TemplateParameter("1"), //TODO: bring some kind of context as 1 isn't always required
      TemplateParameter("2"),
      TemplateParameter("lang"),
    ),
    parameters) {

  /**
   * Le titre de la page visée (par défaut l’article vedette).
   */
  val titre: Option[String] = params.get("1") // TODO: get the page name?
  /**
   * L’étiquette du lien (par défaut la même chose qu’en 1).
   */
  val texte: Option[String] = params.get("2").orElse(titre)
  /**
   * Le code langue de la Wikipédia visée (par défaut fr).
   */
  val lang: Option[String] = params.get("lang")

  override def format(): String = {
    texte match {
      case Some(article) => s"[[$article]]"
      case None => ""
    }
  }
}
