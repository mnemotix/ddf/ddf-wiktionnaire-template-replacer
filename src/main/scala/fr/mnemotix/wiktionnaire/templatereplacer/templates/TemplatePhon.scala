package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.{Template, TemplateParameter}

/**
 * Modèle:phon
 *
 * @param parameters
 */
case class TemplatePhon(parameters: Map[String, String])
  extends Template(
    List(
      TemplateParameter("1", required=true),
      TemplateParameter("2", List("lang"))
    ),
    parameters) {

  /**
   * La prononciation.
   */
  val prononciation: String = params("1")

  override def format(): String = prononciation
}
