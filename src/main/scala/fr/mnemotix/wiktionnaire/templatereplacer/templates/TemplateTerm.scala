package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.{Template, TemplateParameter}

/**
 * Modèle:term
 * @param parameters
 */
case class TemplateTerm(parameters: Map[String, String])
  extends Template(
    List(
      TemplateParameter("1", required=true), // There are more, but we use only this one
    ),
    parameters) {

  /**
   * Le contexte (la terminologie auquel appartient le mot défini)
   */
  val contexte: String = params("1")

  override def format(): String = s"(''${contexte.capitalize}'')"
}
