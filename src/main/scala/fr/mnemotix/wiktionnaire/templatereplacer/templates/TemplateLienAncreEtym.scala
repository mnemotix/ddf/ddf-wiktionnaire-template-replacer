package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.utils.{BooleanLiterals, WiktionarySections}
import fr.mnemotix.wiktionnaire.templatereplacer.{Template, TemplateParameter}

/**
 * Modèle:lien-ancre-étym
 *
 * @param parameters
 */
case class TemplateLienAncreEtym(parameters: Map[String, String])
  extends Template(
    List(
      TemplateParameter("1", required = true),
      TemplateParameter("2", required = true),
      TemplateParameter("3"),
      TemplateParameter("locution")
    ),
    parameters) {

  /**
   * Code langue de la section
   */
  val lang: String = params("1") //ignored
  /**
   * Classe grammaticale du mot vers laquelle lier.
   * Les valeurs acceptées sont les même que le modèle « S » pour les sections de types de mot.
   */
  val section: String = params("2")
  /**
   * S’il y a plusieurs sections de même classe grammaticale, le numéro indiqué par le paramètre « num » du modèle « S »
   */
  val sectionNumber: Option[String] = params.get("3") //ignored
  /**
   * Indique si la section est une locution ou non.
   * Par défaut : non
   */
  val locution: Boolean = BooleanLiterals.toBoolean(params.getOrElse("locution", "non"))

  // DDFS-100: sectionNumber is ignored as per DDF's formatting
  override def format(): String = s"''(${WiktionarySections.toDisplayName(section, locution)})''"
}
