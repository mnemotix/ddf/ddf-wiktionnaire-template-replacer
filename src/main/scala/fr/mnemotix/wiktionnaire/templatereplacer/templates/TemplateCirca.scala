package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.{Template, TemplateParameter}

/**
 * Modèle:circa
 *
 * @param parameters
 */
case class TemplateCirca(parameters: Map[String, String])
  extends Template(
    List(
      TemplateParameter("1", required=true),
    ),
    parameters) {

  /**
   * La date
   */
  val date: String = params("1")

  override def format(): String = s"(c. $date)"
}
