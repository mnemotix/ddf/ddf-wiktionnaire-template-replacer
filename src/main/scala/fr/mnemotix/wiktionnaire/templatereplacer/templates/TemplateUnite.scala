package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.{Template, TemplateParameter}

import java.text.NumberFormat
import java.util.Locale

/**
 * Modèle:Unité
 *
 * @param parameters
 */
case class TemplateUnite(parameters: Map[String, String])
  extends Template(
    List(
      TemplateParameter("1", required = true),
      TemplateParameter("e"),
      TemplateParameter("2"), // unit 1
      TemplateParameter("3"), // exponent 1
      TemplateParameter("4"), // unit 2
      TemplateParameter("5"), // exponent 2
      TemplateParameter("6"), // unit 3
      TemplateParameter("7"), // exponent 3
      TemplateParameter("8"), // unit 3
      TemplateParameter("9"), // exponent 3
    ),
    parameters) {

  /**
   * Valeur à afficher.
   */
  val valeur: String = params("1")

  /**
   * Puissance de 10 à afficher.
   */
  val power10: Option[String] = params.get("e")

  /**
   * Toutes les unités sous la forme d'un couple (symbole, exposant)
   */
  val allUnits: List[(String, Option[String])] = (2 to 8 by 2)
    .map(i => (params.get(i.toString), params.get((i+1).toString)))
    .filter(_._1.isDefined)
    .map(t => (t._1.get, t._2))
    .toList

  override def format(): String = {
    formatNumber() + {
      if (power10.isDefined) s"{{x10|${power10.get}}}"
      else ""
    } + {
      if (allUnits.nonEmpty) " " + formatUnit(allUnits.head) + joinUnits(allUnits.tail)
      else ""
    }
  }

  def formatNumber(): String = {
    // Okay, it's weird.
    // Java does not like localized parsing and formatting
    // On the Wikt, we use periods as decimal separators, but most of the time we use commas, even when knowing that they
    // "don't" work for number formatting.
    // But here, I do the opposite : I turn dots into commas.
    // it's prone to issues, but I'm fed up with it - and I don't want to create a number formatter by myself.
    val formatter = NumberFormat.getNumberInstance(Locale.FRANCE)
    formatter.setMaximumFractionDigits(340) // Why 340? https://stackoverflow.com/a/25308216
    val n: Number = formatter.parse(valeur.replace('.', ','))
    formatter.format(n)
  }

  private def joinUnits(remainingUnits: List[(String, Option[String])]): String = {
    remainingUnits match {
      case Nil => ""
      case last :: Nil => "⋅" + formatUnit(last)
      case h :: t => "⋅" + formatUnit(h) + joinUnits(t)
    }
  }

  /**
   * Formats the (symbole, exposant) tuple.
   * @param tuple the (symbole, exposant) tuple to format
   * @return
   */
  private def formatUnit(tuple: (String, Option[String])): String = {
    tuple._1 + {
      if (tuple._2.isDefined) s"{{e|${tuple._2.get}}}"
      else ""
    }
  }
}
