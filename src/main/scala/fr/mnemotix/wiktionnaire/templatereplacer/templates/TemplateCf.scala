package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.{Template, TemplateParameter}

/**
 * Modèle:cf
 * @param parameters
 */
case class TemplateCf(parameters: Map[String, String])
  extends Template(
    List(
      // at least these two, but can go from "1" to "16"
      TemplateParameter("1"),
      TemplateParameter("2"),
      TemplateParameter("3"),
      TemplateParameter("4"),
      TemplateParameter("5"),
      TemplateParameter("6"),
      TemplateParameter("7"),
      TemplateParameter("8"),
      TemplateParameter("9"),
      TemplateParameter("10"),
      TemplateParameter("11"),
      TemplateParameter("12"),
      TemplateParameter("13"),
      TemplateParameter("14"),
      TemplateParameter("15"),
      TemplateParameter("16"),
      TemplateParameter("lang")
    ),
    parameters) {

  val allWords: List[String] = (1 to 16).map(i => params.get(i.toString)).filter(_.isDefined).map(_.get).toList

  override def format(): String = {
    // If there's no params, it only returns "→ voir"
    // otherwise, it lists all the words
    if (allWords.isEmpty) {
      "→ voir"
    } else {
      "→ voir " + "[[" + allWords.head + "]]" + joinWords(allWords.tail)
    }
  }

  private def joinWords(remainingWords: List[String]): String = {
    remainingWords match {
      case Nil => ""
      case last :: Nil => s" et [[$last]]"
      case h :: t => s", [[$h]]" + joinWords(t)
    }
  }
}
