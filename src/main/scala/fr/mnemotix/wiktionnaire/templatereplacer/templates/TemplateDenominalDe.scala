package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.{Template, TemplateParameter}

/**
 * Modèle:dénominal de
 *
 * This template is marked as obsolete on the Wiktionnaire, so it might be removed in the future, although it is seemingly still used.
 *
 * @param parameters
 */
case class TemplateDenominalDe(parameters: Map[String, String])
  extends Template(
    List(
      TemplateParameter("1", required=true),
      TemplateParameter("2", aliases=List("lang")), // lang, ignored
    ),
    parameters) {

  /**
   * Le mot ou la locution à l'origine du mot vedette.
   */
  val de: String = params("1")

  override def format(): String = s"Dénominal de [[$de]]"
}
