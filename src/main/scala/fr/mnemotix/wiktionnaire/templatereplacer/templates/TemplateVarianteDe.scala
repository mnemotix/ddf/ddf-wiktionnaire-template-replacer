package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.{Template, TemplateParameter}

case class TemplateVarianteDe(parameters: Map[String, String]) extends Template(
  List(
    TemplateParameter("1", required = true),
    TemplateParameter("2"),
    TemplateParameter("3"),
    TemplateParameter("dif"),
    TemplateParameter("tr"),
    TemplateParameter("sens")
  ),
  parameters) {

  /**
   * Graphie du mot
   */
  val mot: String = params("1")
  /**
   * Optionnel. Le code de la langue. Le défaut est fr (le français).
   */
  val lang: String = params.getOrElse("2", "fr")
  /**
   * Optionnel. La section à laquelle le lien doit pointer.
   */
  val section: Option[String] = params.get("3")
  /**
   * Optionnel. Texte à afficher à la place du mot.
   */
  val dif: Option[String] = params.get("dif")
  val tr: Option[String] = params.get("tr")
  val sens: Option[String] = params.get("sens")

  /**
   * Returns the text the template should be replaced with
   *
   * @return the text the template should be replaced with
   */
  override def format(): String = s"Variante de {{lien|$mot|$lang|${section.getOrElse("")}|dif=${dif.getOrElse("")}|tr=${tr.getOrElse("")}|sens=${sens.getOrElse("")}}}"
}


