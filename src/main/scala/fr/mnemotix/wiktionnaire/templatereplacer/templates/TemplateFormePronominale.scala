package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.{Template, TemplateParameter}

case class TemplateFormePronominale(parameters: Map[String, String])
extends Template(
  List(
    TemplateParameter("1", required=true),
    TemplateParameter("lang")
  ),
  parameters) {

  /**
   * Forme à l'infinitif du verbe
   */
  val infinitif: String = params("1")

  /**
   * Code de langue de la cible du lien
   */
  val lang: Option[String] = params.get("lang")

  override def format(): String = s"Forme pronominale de [[$infinitif]]"
}
