package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.{Template, TemplateParameter}

case class TemplatePolytonique(parameters: Map[String, String])
  extends Template(
    List(
      TemplateParameter("1", required=true),
      TemplateParameter("2", List("tr")),
      TemplateParameter("3", List("sens"))
    ),
    parameters) {

  /**
   * Le mot vers lequel le lien doit pointer. Peut contenir des balises de formatage.
   */
  val mot: String = params("1")
  /**
   * La transcription latine du mot
   */
  val transcript: Option[String] = params.get("2")
  /**
   * Sa signification française
   */
  val sens: Option[String] = params.get("3")

  override def format(): String = mot + {
    if (transcript.isDefined) ", " + transcript.get
    else ""
  } + {
    if (sens.isDefined) s" (« ${sens.get} »)"
    else ""
  }
}
