package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.Template

/**
 * Correspond à un modèle explicitement ignoré (donc non affiché).
 */
case class TemplateIgnored() extends Template(List(), Map()) {
  override def format(): String = "" // Prints out nothing
}
